# Knative Nodejs functions

This project is a set of Knative [functions](https://docs.gitlab.com/ee/user/project/clusters/serverless/#deploying-functions) using [GitLab-managed runtimes](https://docs.gitlab.com/ee/user/project/clusters/serverless/#gitlab-managed-runtimes)

> These functions are based on the [Riff project](https://projectriff.io/)

To deploy these functions:

- You must activate the [Kubernetes integration](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html) in GitLab
- Then, you need to install [Knative via GitLab’s Kubernetes integration](https://docs.gitlab.com/ee/user/project/clusters/serverless/#installing-knative-via-gitlabs-kubernetes-integration)
- You can use [your own installation of Knative](https://docs.gitlab.com/ee/user/project/clusters/serverless/#using-an-existing-installation-of-knative)

When the CI/CD pipeline is complete, you can test the functions with `POST` requests:

> - You can retrieve the url of each functions by selecting the `Operations/Serverless` menu

### hello function

```bash
curl -d '{"name":"Bob Morane"}' \
-H "Content-Type: application/json" \
-X POST http://functions-hello.${CI_PROJECT_NAME}-${CI_PROJECT_ID}-${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
```

> Result:
> `🇬🇧 Hello Bob Morane`

### bonjour function

```bash
curl -d '{"name":"Bob Morane"}' \
-H "Content-Type: application/json" \
-X POST http://functions-bonjour.${CI_PROJECT_NAME}-${CI_PROJECT_ID}-${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
```

> Result:
> `🇫🇷 Bonjour Bob Morane`

### morgen function

```bash
curl -d '{"name":"Bob Morane"}' \
-H "Content-Type: application/json" \
-X POST http://functions-morgen.${CI_PROJECT_NAME}-${CI_PROJECT_ID}-${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
```

> Result:
> `🇩🇪 Morgen Bob Morane`
